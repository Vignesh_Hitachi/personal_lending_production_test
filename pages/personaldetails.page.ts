import { Selector, t } from 'testcafe';

const plender = require('../config/plender.json');


export const personalDetailsPage = {
   
    //Elements    
    personalDetailsHeader: Selector('h2').withText('Personal Details'),

    firstName: Selector("input[name='firstName']"),
    lastName: Selector("input[name='lastName']"),
    postcode: Selector("input[name='postcode']"),
    lookupbutton: Selector("button[data-test-id='lookup-postcode-button']"),
    addressInput: Selector("select[name='address']"),
    saveNextButton: Selector("button[data-test-id='proceed-button']"),

    //negative scenarios
    invalidPostcode: Selector("div[data-test-id='postcode-error']"),

    
    async fillName() {
        await t.typeText( this.firstName, plender.firstName )
        await t.typeText( this.lastName, plender.lastName )
        await t.typeText( this.postcode, plender.postcode )
         },

    async checkPostcode(postcode: string){
        await t.typeText( this.firstName, plender.firstName )
        await t.typeText( this.lastName, plender.lastName )
        await t.typeText( this.postcode, postcode)

    },
    
    async verifyInvalidPcode(){
        const invalid = this.invalidPostcode.innerText;
        await t.expect(invalid).eql('Invalid postcode');

    },
   
    async requiredPostcode(){
        const invalid = this.invalidPostcode.innerText;
        await t.expect(invalid).eql('Required');
    }, 

    async notFoundPostcode(){
        const invalid = this.invalidPostcode.innerText;
        await t.expect(invalid).eql('Not found');
    }, 

        
    async selectPostCode(pcode: number) { 
        const postcodeOption = this.addressInput.find(`option[value="${pcode}"]`);
        await t.click(this.addressInput);
        await t.click(postcodeOption);
        
         },
    async clickLookUp() { await t.click(this.lookupbutton) },

    async clickSaveNext() { await t.click(this.saveNextButton) }  



};