import { Selector, t } from 'testcafe';
import { basePage } from './base.page';

const plender = require('../config/plender.json');


export const hiPage = {
    url: `${basePage.url}`,
    	
    //Quote Elements
    
    home_xpath: Selector('a').withAttribute('href', '/home-improvement-loans/'),
    apply_now: Selector('[class="button button--applynow button--full-width"]'),
    accept_all: Selector('[id="ccc-notify-accept"]'),

    async clickAcceptAll() { await t.click(this.accept_all) },
    
    async clickHomeImprovement() { await t.click(this.home_xpath) },

    async clickApplyNow() { await t.click(this.apply_now) }
    
};