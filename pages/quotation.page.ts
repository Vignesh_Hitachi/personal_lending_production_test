import { Selector, t } from 'testcafe';
import { basePage } from './base.page';

const plender = require('../config/plender.json');


export const quotationPage = {
    url: `${basePage.url}`,
    	
    //Quote Elements
    
    amountInput: Selector('[name="amount"]'),
    termInput: Selector('[name="term"]'),
    proceed: Selector("button[data-test-id='proceed-button']"),  
    creditMsg: Selector('[class="css-fvzcrf"]'),
    amountError: Selector("div[data-test-id='amount-error']"),
    termError: Selector("div[data-test-id='term-error']"),

    async fillForm(amount: number, term: number) {
        const termOption = this.termInput.find(`option[value="${term}"]`);
        await t.typeText( this.amountInput, `${amount}` )
         .click(this.termInput)
         .click(termOption);
      },
    
    async validateForm(amount: string, term: string) {
       
        await t.typeText( this.amountInput, `${amount}` )
        await t.click(this.termInput)
         
      },  
     
    async verifyAmountError(){
        const amountErrorMsg = this.amountError.innerText;
        return amountErrorMsg
      
    },

    async verifyCreditMessage(){
        const creditMsg = this.creditMsg.innerText;
        return creditMsg
      
    },


    async verifyTermError(){
        const termErrorMsg = this.termError.innerText;
        return termErrorMsg


    },

    async verifyTooLarge(){
        const amountErrorMsg = this.amountError.innerText;
        await t.expect(amountErrorMsg).eql('Too large');
    },

    async clickProceed() { await t.click(this.proceed) }
    
};