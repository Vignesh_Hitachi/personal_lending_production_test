export const getRandomEmail = (): string => {
    const chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
    var email = '';
    for (var i = 0; i < 8; i++) {
        email += chars[Math.floor(Math.random() * chars.length)];
    }

    return `${email}@email.com`;
};

export const dataTestId = (name: string) => `[data-test-id="${name}"]`;

export const formatAddress = async (addressLines: Selector) => {
  const count = await addressLines.count;

  const address: string[] = [];

  for (let i = 0; i < count; i++) {
    const text = await addressLines.nth(i).innerText;
    address.push(text);
  };

  return address.join(', ');
};

