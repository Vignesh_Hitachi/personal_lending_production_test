import { axeCheck, createReport } from 'axe-testcafe';

import { quotationPage } from '../pages/quotation.page';
import { hiPage } from '../pages/homeimp.page';
import { personalDetailsPage } from '../pages/personaldetails.page';
import { basePage } from '../pages/base.page';



import { testAccessibility } from '../utils/axe.utils';

fixture('Testing Plending Application on Cloudfront URL')
    .page(basePage.url);

test('Testing Plending App. on the Cloudfront ', async (t) => {

    
    await t.maximizeWindow()
    await hiPage.clickAcceptAll()
    //await t.debug();
    await hiPage.clickHomeImprovement()
    await hiPage.clickAcceptAll()
    await hiPage.clickApplyNow()
    

    //Quotations Page
    await quotationPage.fillForm(5000,36)
    //await testAccessibility(t, 'quotation');
    await t
		.expect(quotationPage.creditMsg.innerText)
		.contains('Credit is subject to status and an assessment of your financial circumstances.')
    await quotationPage.clickProceed()

    //PersonalDetailsPage
    await personalDetailsPage.fillName()
   

    
});


