import { axeCheck, createReport } from 'axe-testcafe';
import { basePage } from '../pages/base.page';
import { quotationPage } from '../pages/quotation.page';


fixture('Negative scenarios for quotations page')
    .page(basePage.url);

test('Invalid scenarios', async t => {
    
    await t.maximizeWindow();
    await quotationPage.validateForm("String","36");
 	//await testAccessibility(t, 'quotation');
    await t.expect(await quotationPage.verifyAmountError()).eql('Required');
    await t.pressKey('tab')

    await t.expect(await quotationPage.verifyTermError()).eql('Required');

    await quotationPage.validateForm("50000000","36");
    await t.expect(await quotationPage.verifyAmountError()).eql('Too large');
    //await quotationPage.verifyTooLarge()

});

// test('Postcode Required ', async (t) => {
    
//     await t.maximizeWindow();
//     await quotationPage.fillForm(5000,36);
//  	//await testAccessibility(t, 'quotation');
//     await quotationPage.clickProceed();


// }),

// test('Postcode not found ', async (t) => {
    
//     await t.maximizeWindow();
//     await quotationPage.fillForm(5000,36);
//  	//await testAccessibility(t, 'quotation');
//     await quotationPage.clickProceed();

    
// });
