export interface Config {
    baseUrl: string;
  }
  ​
  export const getConfig = () => {
    switch (process.env.TEST_ENV) {
      case 'dev':
        return devConfig;
      case 'sit':
        return sitConfig;
      default:
        return prodConfig;
    };
  };
  ​
  const devConfig: Config = {
    baseUrl: 'dev-url'
  };
  ​
  const sitConfig: Config = {
    baseUrl: 'sit-url'
  };
  ​
  const prodConfig: Config = {
    baseUrl: 'prod-url'
  }